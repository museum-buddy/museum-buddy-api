#!/bin/bash

APP_NAME="museum-buddy-api"
CON_NAME="mb-api"

go mod tidy 
go build -tags netgo -a -v -o $APP_NAME cmd/$APP_NAME/main.go
docker stop $CON_NAME
docker rm $CON_NAME
docker build -t $CON_NAME:development -f build/package/Dockerfile .
docker run --name $CON_NAME --network host -p 8080:8080 $CON_NAME:development
