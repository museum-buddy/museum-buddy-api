package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/caarlos0/env/v7"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/config"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/router"
)

func main() {
	var cfg config.Config
	if err := env.Parse(&cfg); err != nil {
		fmt.Println("couldn't parse environment:", err.Error())
		os.Exit(1)
	}
	config.Values = &cfg
	if err := logger.SetLogLevel(config.Values.LogLevel); err != nil {
		fmt.Println("couldn't set log level to ", config.Values.LogLevel)
	}

	r := router.InitRoutes()
	addr := fmt.Sprintf("%s:%d", config.Values.APIHost, config.Values.APIPort)

	srv := &http.Server{
		Handler:      r,
		Addr:         addr,
		WriteTimeout: 120 * time.Second,
		ReadTimeout:  120 * time.Second,
	}
	logger.Info("Now listening on %s", addr)
	if err := srv.ListenAndServe(); err != nil {
		logger.Error("failed to start api: %s", err)
	}
}
