module gitlab.com/museum-buddy/museum-buddy-api

go 1.20

require (
	github.com/caarlos0/env/v7 v7.0.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/logrusorgru/aurora/v4 v4.0.0
	github.com/mithrandie/csvq-driver v1.6.9
)

require (
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	github.com/mithrandie/csvq v1.17.11 // indirect
	github.com/mithrandie/go-file/v2 v2.1.0 // indirect
	github.com/mithrandie/go-text v1.5.4 // indirect
	github.com/mithrandie/ternary v1.1.1 // indirect
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa // indirect
	golang.org/x/sys v0.0.0-20220615213510-4f61da869c0c // indirect
	golang.org/x/term v0.0.0-20201126162022-7de9c90e9dd1 // indirect
	golang.org/x/text v0.3.7 // indirect
)
