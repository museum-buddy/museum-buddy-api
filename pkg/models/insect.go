package models

import "gitlab.com/museum-buddy/museum-buddy-api/pkg/util"

// NHInsect contains all the fields that an insect needs
type NHInsect struct {

	//See: NHObject
	ID        int64  `db:"ID" json:"id"`
	Name      string `db:"NAME" json:"name"`
	SellValue int    `db:"SELL_VALUE" json:"sellValue"`
	Type      string `db:"TYPE" json:"type"`

	//See: NHCritter
	CritterID   int64  `db:"CRITTER_ID" json:"critterId"`
	Location    string `db:"LOCATION" json:"location"`
	NeedsRain   bool   `db:"NEEDS_RAIN" json:"needsRain"`
	Hours       string `db:"HOURS" json:"hours"`
	NorthMonths string `db:"NORTH_MONTHS" json:"northMonths"`
	SouthMonths string `db:"SOUTH_MONTHS" json:"southMonths"`
}

// GetID returns the ID
func (o NHInsect) GetID() int64 {
	return o.ID
}

// GetName returns the Name
func (o NHInsect) GetName() string {
	return o.Name
}

// GetSellValue returns the SaleValue
func (o NHInsect) GetSellValue() int {
	return o.SellValue
}

// GetType returns the Type
func (o NHInsect) GetType() string {
	return o.Type
}

// GetCritterID returns the CritterID
func (o NHInsect) GetCritterID() int64 {
	return o.CritterID
}

// GetNorthMonths returns the NorthMonths
func (o NHInsect) GetNorthMonths() []int {
	return util.Splint(o.NorthMonths)
}

// GetSouthMonths returns the SouthMonths
func (o NHInsect) GetSouthMonths() []int {
	return util.Splint(o.SouthMonths)
}

// GetHours returns the Months
func (o NHInsect) GetHours() []int {
	return util.Splint(o.Hours)
}

// GetLocation returns the Location
func (o NHInsect) GetLocation() string {
	return o.Location
}

// GetNeedsRain returns the NeedsRain
func (o NHInsect) GetNeedsRain() bool {
	return o.NeedsRain
}
