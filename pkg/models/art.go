package models

import "strings"

// NHArt describes art
type NHArt struct {

	//See: NHObject
	ID        int64  `db:"ID" json:"id"`
	Name      string `db:"NAME" json:"name"`
	SellValue int    `db:"SELL_VALUE" json:"sellValue"`
	Type      string `db:"TYPE" json:"type"`

	RealName     string `db:"REAL_NAME" json:"realName"`
	RealArtist   string `db:"REAL_ARTIST" json:"realArtist"`
	ForgeryHints string `db:"FORGERY" json:"forgeryHints"`
}

// GetID returns the ID
func (o NHArt) GetID() int64 {
	return o.ID
}

// GetName returns the Name
func (o NHArt) GetName() string {
	return o.Name
}

// GetSellValue returns the SaleValue
func (o NHArt) GetSellValue() int {
	return o.SellValue
}

// GetType returns the Type
func (o NHArt) GetType() string {
	return o.Type
}

// GetRealArtist returns the real artist
func (o NHArt) GetRealArtist() string {
	return o.RealArtist
}

// GetRealName returns the real name
func (o NHArt) GetRealName() string {
	return o.RealName
}

// GetForgeryHints returns the forgery hints
func (o NHArt) GetForgeryHints() []string {
	return strings.Split(o.ForgeryHints, ";")
}
