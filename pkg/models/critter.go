package models

// NHCritterI is the interface for a new horizons critter
type NHCritterI interface {
	GetCritterID() int64
	GetLocation() string
	GetNeedsRain() bool
	GetHours() []int
	GetNorthMonths() []int
	GetSouthMonths() []int
}
