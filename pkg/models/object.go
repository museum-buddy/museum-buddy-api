package models

// NHObjectI is the interface for NHObject
type NHObjectI interface {
	GetID() int64
	GetName() string
	GetSellValue() int
	GetType() string
}
