package models

import "gitlab.com/museum-buddy/museum-buddy-api/pkg/util"

// NHFish contains all the fields that a fish needs
type NHFish struct {

	//See: NHObject
	ID        int64  `db:"ID" json:"id"`
	Name      string `db:"NAME" json:"name"`
	SellValue int    `db:"SELL_VALUE" json:"sellValue"`
	Type      string `db:"TYPE" json:"type"`

	//See: NHCritter
	CritterID   int64  `db:"CRITTER_ID" json:"critterId"`
	Location    string `db:"LOCATION" json:"location"`
	NeedsRain   bool   `db:"NEEDS_RAIN" json:"needsRain"`
	Hours       string `db:"HOURS" json:"hours"`
	NorthMonths string `db:"NORTH_MONTHS" json:"northMonths"`
	SouthMonths string `db:"SOUTH_MONTHS" json:"southMonths"`

	ShadowSize string `db:"SHADOW_SIZE" json:"shadowSize"`
	HasFin     bool   `db:"HAS_FIN" json:"hasFin"`
}

// GetID returns the ID
func (o NHFish) GetID() int64 {
	return o.ID
}

// GetName returns the Name
func (o NHFish) GetName() string {
	return o.Name
}

// GetSellValue returns the SellValue
func (o NHFish) GetSellValue() int {
	return o.SellValue
}

// GetType returns the Type
func (o NHFish) GetType() string {
	return o.Type
}

// GetCritterID returns the CritterID
func (o NHFish) GetCritterID() int64 {
	return o.CritterID
}

// GetNorthMonths returns the NorthMonths
func (o NHFish) GetNorthMonths() []int {
	return util.Splint(o.NorthMonths)
}

// GetSouthMonths returns the SouthMonths
func (o NHFish) GetSouthMonths() []int {
	return util.Splint(o.SouthMonths)
}

// GetHours returns the Months
func (o NHFish) GetHours() []int {
	return util.Splint(o.Hours)
}

// GetLocation returns the Location
func (o NHFish) GetLocation() string {
	return o.Location
}

// GetNeedsRain returns the NeedsRain
func (o NHFish) GetNeedsRain() bool {
	return o.NeedsRain
}

// GetShadowSize returns the ShadowSize
func (o NHFish) GetShadowSize() string {
	return o.ShadowSize
}

// GetHasFin returns the HasFin
func (o NHFish) GetHasFin() bool {
	return o.HasFin
}
