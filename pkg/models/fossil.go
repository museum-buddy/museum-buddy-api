package models

// NHFossil contains the information needed for Fossils
type NHFossil struct {
	ID        int64  `db:"ID" json:"id"`
	Name      string `db:"NAME" json:"name"`
	SellValue int    `db:"SELL_VALUE" json:"sellValue"`
	Type      string `db:"TYPE" json:"type"`
}

// GetID returns the ID
func (o NHFossil) GetID() int64 {
	return o.ID
}

// GetName returns the Name
func (o NHFossil) GetName() string {
	return o.Name
}

// GetSellValue returns the SaleValue
func (o NHFossil) GetSellValue() int {
	return o.SellValue
}

// GetType returns the Type
func (o NHFossil) GetType() string {
	return o.Type
}
