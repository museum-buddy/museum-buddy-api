package models

import "gitlab.com/museum-buddy/museum-buddy-api/pkg/util"

// NHSeaCritter contains all the fields that a sea critter needs
type NHSeaCritter struct {

	//See: NHObject
	ID        int64  `db:"ID" json:"id"`
	Name      string `db:"NAME" json:"name"`
	SellValue int    `db:"SELL_VALUE" json:"sellValue"`
	Type      string `db:"TYPE" json:"type"`

	//See: NHCritter
	CritterID   int64  `db:"CRITTER_ID" json:"critterId"`
	Hours       string `db:"HOURS" json:"hours"`
	NorthMonths string `db:"NORTH_MONTHS" json:"northMonths"`
	SouthMonths string `db:"SOUTH_MONTHS" json:"southMonths"`

	ShadowSize string `db:"SHADOW_SIZE" json:"shadowSize"`
	Speed      int64  `db:"SPEED" json:"speed"`
}

// GetID returns the ID
func (o NHSeaCritter) GetID() int64 {
	return o.ID
}

// GetName returns the Name
func (o NHSeaCritter) GetName() string {
	return o.Name
}

// GetSellValue returns the SellValue
func (o NHSeaCritter) GetSellValue() int {
	return o.SellValue
}

// GetType returns the Type
func (o NHSeaCritter) GetType() string {
	return o.Type
}

// GetCritterID returns the CritterID
func (o NHSeaCritter) GetCritterID() int64 {
	return o.CritterID
}

// GetNorthMonths returns the NorthMonths
func (o NHSeaCritter) GetNorthMonths() []int {
	return util.Splint(o.NorthMonths)
}

// GetSouthMonths returns the SouthMonths
func (o NHSeaCritter) GetSouthMonths() []int {
	return util.Splint(o.SouthMonths)
}

// GetHours returns the Months
func (o NHSeaCritter) GetHours() []int {
	return util.Splint(o.Hours)
}

// GetLocation returns the Location
func (o NHSeaCritter) GetLocation() string {
	return "Sea"
}

// GetShadowSize returns the ShadowSize
func (o NHSeaCritter) GetShadowSize() string {
	return o.ShadowSize
}

// GetSpeed returns the Speed
func (o NHSeaCritter) GetSpeed() int64 {
	return o.Speed
}
