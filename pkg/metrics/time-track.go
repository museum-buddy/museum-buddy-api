package metrics

import (
	"fmt"
	"strconv"
	"time"

	"github.com/logrusorgru/aurora/v4"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/constants"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/runtime"
)

// Level is the log level to log to when tracking time
var Level string = logger.LogLevelInfo

// Start just returns the current time
func Start() time.Time {
	return time.Now()
}

// End prints the duration to the configured log
func End(start time.Time, label ...string) (elapsed time.Duration) {
	elapsed = time.Since(start)
	callerDepth := 3
	l := constants.EmptyString
	useMyCaller := true
	if len(label) > 0 {
		l = label[0]
		if l != constants.EmptyString {
			l = fmt.Sprintf("[%s]", l)
		}
	}
	if len(label) > 1 {
		if b, err := strconv.ParseBool(label[1]); err != nil {
			logger.Log(logger.LogLevelWarning, "couldn't parse useMyCaller flag %s", err)
		} else {
			useMyCaller = b
		}
	}
	if len(label) > 2 {
		if d, err := strconv.Atoi(label[2]); err != nil {
			logger.Log(logger.LogLevelWarning, "couldn't parse callerDepth %s", err)
		} else {
			callerDepth = d
		}
	}
	msg := "%s took %s to finish"
	var vars []interface{}
	if useMyCaller {
		myCaller := runtime.MyCaller(callerDepth)
		msg = fmt.Sprintf("%%s %s", msg)
		vars = []interface{}{l, aurora.Blue(myCaller), aurora.Cyan(elapsed)}
	} else {
		vars = []interface{}{l, aurora.Cyan(elapsed)}
	}
	logger.Log(Level, msg, vars...)
	return
}
