package config

import (
	"github.com/jmoiron/sqlx"
	// This is here because it has to be
	_ "github.com/mithrandie/csvq-driver"
)

// Values is the currently loaded config definition
var Values *Config

// Config holds the config definiting for this app
type Config struct {
	LogLevel string `env:"LOG_LEVEL" envDefault:"DEBUG"`

	APIHost    string `env:"API_HOST" envDefault:"localhost"`
	APIPort    int    `env:"API_PORT" envDefault:"8080"`
	APIEnv     string `env:"API_ENV" envDefault:"local"`
	APIVersion string `envDefault:"dev-0.0.0"`

	CrossDomainAllowedOrigins string `env:"CROSS_DOMAIN_ALLOWED_ORIGINS" envDefault:"*"`
	CrossDomainAllowedHeaders string `env:"CROSS_DOMAIN_ALLOWED_HEADERS" envDefault:"*"`

	DBDriverName string `env:"DB_DRIVER_NAME" envDefault:"csvq"`
}

// Connect will connect to the csv db folder
func Connect() (db *sqlx.DB, err error) {
	return sqlx.Open(Values.DBDriverName, "assets")
}
