package fossils

import (
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/config"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/constants"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/models"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/runtime"
)

// QuerySelectFossils contains the necessary information for executing a select fossils query
type QuerySelectFossils struct {
	Donated    *bool
	DonatedIds []string

	Sort   *string
	Dir    *string
	Limit  *int
	Offset *int
}

// Execute executes the query
func (q *QuerySelectFossils) Execute() (fossils []models.NHFossil, err error) {
	var db *sqlx.DB
	db, err = config.Connect()
	if err != nil {
		logger.Error("couldn't connect to db: %s", err)
		return
	}

	qs := q.getQueryString(q.Sort, q.Dir, q.Limit, q.Offset)

	logger.Debug("EXECUTING\n%s", qs)

	err = db.Select(&fossils, qs)

	if err != nil {
		logger.Error("error selecting fossils: %s", err.Error())
	} else if len(fossils) == 0 {
		fossils = make([]models.NHFossil, 0)
	}

	return
}

// GetTotal runs the same query without limits, offsets, or sorting, to get the total number of results for pagination
func (q *QuerySelectFossils) GetTotal() (total int, err error) {
	var db *sqlx.DB
	db, err = config.Connect()
	if err != nil {
		logger.Error("couldn't connect to db: %s", err)
		return
	}

	qs := q.getQueryString(nil, nil, nil, nil)

	qs = fmt.Sprintf("SELECT COUNT(*) FROM (%s) _", qs)

	err = db.Get(&total, qs)

	if err != nil {
		me := runtime.MyCaller()
		logger.Error("error getting total from %s: %s", me, err)
	}
	return
}

func (q *QuerySelectFossils) getQueryString(s *string, d *string, l *int, o *int) string {
	var sb strings.Builder
	sb.WriteString("SELECT * FROM `nh-fossils.csv`")
	woa := " WHERE "
	if q.Donated != nil && q.DonatedIds != nil {
		sb.WriteString(woa + " ID ")
		if !*q.Donated {
			sb.WriteString(" NOT ")
		}
		sb.WriteString(" IN (")
		if q.DonatedIds != nil {
			sb.WriteString(strings.Join(q.DonatedIds, constants.Comma))
		} else {
			sb.WriteString("-1")
		}
		sb.WriteString(") ")
		woa = " AND "
	}
	if s != nil && d != nil {
		sb.WriteString(" ORDER BY " + *s + constants.Space + *d)
	} else {
		sb.WriteString(" ORDER BY ID ASC ")
	}
	if o != nil && l != nil {
		sb.WriteString(fmt.Sprintf(" LIMIT %d OFFSET %d ", *l, *o))
	}
	return sb.String()
}
