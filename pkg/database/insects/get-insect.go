package insects

import (
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/config"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/models"
)

// QueryGetInsect contains the necessary information for executing a select insects query
type QueryGetInsect struct {
	Name      *string
	CritterID *int
}

// Execute executes the query
func (q *QueryGetInsect) Execute() (insect models.NHInsect, err error) {
	var db *sqlx.DB
	db, err = config.Connect()
	if err != nil {
		logger.Error("couldn't connect to db: %s", err)
		return
	}

	qs := q.getQueryString()
	params := q.getParams()

	logger.Debug("EXECUTING\n%s\nwith\n%v", qs, params)

	if len(params) > 0 {
		err = db.Get(&insect, qs, params...)
	} else {
		err = db.Get(&insect, qs)
	}
	if err != nil {
		logger.Error("error getting insect: %s", err.Error())
	}

	return
}

func (q *QueryGetInsect) getQueryString() string {
	var sb strings.Builder
	sb.WriteString("SELECT * FROM `nh-insects.csv` WHERE ")
	if q.CritterID != nil {
		sb.WriteString(" CRITTER_ID = ? ")
	} else {
		sb.WriteString(" NAME = ? ")
	}
	return sb.String()
}

func (q *QueryGetInsect) getParams() []interface{} {
	var params []interface{}
	if q.CritterID != nil {
		params = append(params, *q.CritterID)
	} else {
		params = append(params, *q.Name)
	}
	return params
}
