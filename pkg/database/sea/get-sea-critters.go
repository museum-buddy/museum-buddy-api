package sea

import (
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/config"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/models"
)

// QueryGetSeaCritter contains the necessary information for executing a get fish query
type QueryGetSeaCritter struct {
	Name      *string
	CritterID *int
}

// Execute executes the query
func (q *QueryGetSeaCritter) Execute() (critter models.NHSeaCritter, err error) {
	var db *sqlx.DB
	db, err = config.Connect()
	if err != nil {
		logger.Error("couldn't connect to db: %s", err)
		return
	}

	qs := q.getQueryString()
	params := q.getParams()

	logger.Debug("EXECUTING\n%s\nwith\n%v", qs, params)

	if len(params) > 0 {
		err = db.Get(&critter, qs, params...)
	} else {
		err = db.Get(&critter, qs)
	}
	if err != nil {
		logger.Error("error getting a sea critter: %s", err.Error())
	}

	return
}

func (q *QueryGetSeaCritter) getQueryString() string {
	var sb strings.Builder
	sb.WriteString("SELECT * FROM `nh-sea.csv` WHERE ")
	if q.CritterID != nil {
		sb.WriteString(" CRITTER_ID = ? ")
	} else {
		sb.WriteString(" NAME = ? ")
	}
	return sb.String()
}

func (q *QueryGetSeaCritter) getParams() []interface{} {
	var params []interface{}
	if q.CritterID != nil {
		params = append(params, *q.CritterID)
	} else {
		params = append(params, *q.Name)
	}
	return params
}
