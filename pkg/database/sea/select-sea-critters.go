package sea

import (
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/config"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/constants"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/models"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/runtime"
)

// QuerySelectSeaCritters contains the necessary information for executing a select sea critters query
type QuerySelectSeaCritters struct {
	Month      *int
	Hour       *int
	Caught     *bool
	Donated    *bool
	Hemisphere string
	CaughtIds  []string
	DonatedIds []string

	Sort   *string
	Dir    *string
	Limit  *int
	Offset *int
}

// Execute executes the query
func (q *QuerySelectSeaCritters) Execute() (critters []models.NHSeaCritter, err error) {
	var db *sqlx.DB
	db, err = config.Connect()
	if err != nil {
		logger.Error("couldn't connect to db: %s", err)
		return
	}

	qs := q.getQueryString(q.Sort, q.Dir, q.Limit, q.Offset)
	params := q.getParams()

	logger.Debug("EXECUTING\n%s\nwith\n%v", qs, params)

	if len(params) > 0 {
		err = db.Select(&critters, qs, params...)
	} else {
		err = db.Select(&critters, qs)
	}
	if err != nil {
		logger.Error("error selecting sea critters: %s", err.Error())
	} else if len(critters) == 0 {
		critters = make([]models.NHSeaCritter, 0)
	}

	return
}

// GetTotal runs the same query without limits, offsets, or sorting, to get the total number of results for pagination
func (q *QuerySelectSeaCritters) GetTotal() (total int, err error) {
	var db *sqlx.DB
	db, err = config.Connect()
	if err != nil {
		logger.Error("couldn't connect to db: %s", err)
		return
	}

	qs := q.getQueryString(nil, nil, nil, nil)
	params := q.getParams()

	qs = fmt.Sprintf("SELECT COUNT(*) FROM (%s) _", qs)
	if len(params) > 0 {
		err = db.Get(&total, qs, params...)
	} else {
		err = db.Get(&total, qs)
	}
	if err != nil {
		me := runtime.MyCaller()
		logger.Error("error getting total from %s: %s", me, err)
	}
	return
}

func (q *QuerySelectSeaCritters) getQueryString(s *string, d *string, l *int, o *int) string {
	var sb strings.Builder
	sb.WriteString("SELECT * FROM `nh-sea.csv`")
	woa := " WHERE "
	if q.Caught != nil {
		sb.WriteString(woa + " ID ")
		if !*q.Caught {
			sb.WriteString(" NOT ")
		}
		sb.WriteString(" IN (")
		if q.CaughtIds != nil {
			sb.WriteString(strings.Join(q.CaughtIds, constants.Comma))
		} else {
			sb.WriteString("-1")
		}
		sb.WriteString(") ")
		woa = " AND "
	}
	if q.Donated != nil {
		sb.WriteString(woa + " ID ")
		if !*q.Donated {
			sb.WriteString(" NOT ")
		}

		sb.WriteString(" IN (")
		if q.DonatedIds != nil {
			sb.WriteString(strings.Join(q.DonatedIds, constants.Comma))
		} else {
			sb.WriteString("-1")
		}
		sb.WriteString(") ")

		woa = " AND "
	}
	if q.Hour != nil {
		sb.WriteString(woa + " HOURS LIKE ? ")
		woa = " AND "
	}
	if q.Month != nil {
		sb.WriteString(woa)
		if q.Hemisphere == "south" {
			sb.WriteString(" SOUTH_MONTHS ")
		} else {
			sb.WriteString(" NORTH_MONTHS ")
		}
		sb.WriteString(" LIKE ? ")
		woa = " AND "
	}
	if s != nil && d != nil {
		sb.WriteString(" ORDER BY " + *s + constants.Space + *d)
	} else {
		sb.WriteString(" ORDER BY ID ASC ")
	}
	if o != nil && l != nil {
		sb.WriteString(fmt.Sprintf(" LIMIT %d OFFSET %d ", *l, *o))
	}
	return sb.String()
}

func (q *QuerySelectSeaCritters) getParams() []interface{} {
	var params []interface{}
	if q.Hour != nil {
		h := fmt.Sprintf("%%,%d,%%", *q.Hour)
		params = append(params, h)
	}
	if q.Month != nil {
		m := fmt.Sprintf("%%,%d,%%", *q.Month)
		params = append(params, m)
	}
	return params
}
