package util

import (
	"strconv"
	"strings"
	"time"

	"gitlab.com/museum-buddy/museum-buddy-api/pkg/constants"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
)

// Boolean parses a boolean or doesnt
func Boolean(s string) *bool {
	if b, e := strconv.ParseBool(s); e == nil {
		return &b
	}
	return nil
}

// Datetime parses a time.Time or doesnt
func Datetime(s string) *time.Time {
	if t, e := time.Parse("2006-01-02 15:04:05-0700", s); e != nil {
		logger.Error("coudlnt datetime: %s", e)
	} else {
		return &t
	}
	return nil
}

// Integer parses an integer or doesnt
func Integer(s string) *int {
	if i, e := strconv.Atoi(s); e == nil {
		return &i
	}
	return nil
}

// Str parses a string or doesnt
func Str(s string) *string {
	if s == constants.EmptyString {
		return nil
	}
	return &s
}

// Splint splits a string into an []int on comma
func Splint(s string) []int {
	var si []int
	ss := strings.Split(s, ",")
	for _, sS := range ss {
		if i, e := strconv.Atoi(sS); e == nil {
			si = append(si, i)
		}
	}
	return si
}
