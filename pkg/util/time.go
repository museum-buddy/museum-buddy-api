package util

import (
	"time"

	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
)

func CalculateOffset(systemTime time.Time) string {
	now := time.Now()
	return systemTime.Sub(now).String()
}

func AddOffset(dur string) time.Time {
	now := time.Now()
	d, err := time.ParseDuration(dur)
	if err != nil {
		logger.Error("couldn't parse duration: %s", err)
		return now
	}
	then := now.Add(d)
	return then
}
