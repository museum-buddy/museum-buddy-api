package constants

const (
	// EmptyString is the empty string
	EmptyString = ""
	// Space is a space
	Space = " "
	// Comma is a comma
	Comma = ","
)
