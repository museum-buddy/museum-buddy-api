package router

import (
	"net/http"

	"gitlab.com/museum-buddy/museum-buddy-api/pkg/database/fossils"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/util"
)

// ListFossilsHandler lists out paginated art, is filterable and sortable, etc
func ListFossilsHandler(w http.ResponseWriter, r *http.Request) {
	var j JSONResponse

	qs := r.URL.Query()

	query := fossils.QuerySelectFossils{
		Donated: util.Boolean(qs.Get("donated")),
		Offset:  util.Integer(qs.Get("offset")),
		Limit:   util.Integer(qs.Get("limit")),
		Sort:    util.Str(qs.Get("sort")),
		Dir:     util.Str(qs.Get("dir")),
	}

	if query.Donated != nil {
		query.DonatedIds = qs["donated-id"]
	}

	fossils, err := query.Execute()
	if err != nil {
		j = CreateJSONError(http.StatusInternalServerError, "error selecting fossils: %s", err)
	} else {
		t, err := query.GetTotal()
		if err != nil {
			logger.Error("couldn't get total for ListFossilsHandler: %s", err)
		}
		var o, l int
		if query.Offset != nil {
			o = *query.Offset
		}
		if query.Limit != nil {
			l = *query.Limit
		}
		j = JSONObject{
			Object: PaginationObject{
				Items:  fossils,
				Offset: o,
				Limit:  l,
				Total:  t,
			},
		}
	}

	WriteJSON(w, j)
}
