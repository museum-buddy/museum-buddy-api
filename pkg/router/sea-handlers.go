package router

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/constants"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/database/sea"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/models"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/util"
)

// ListSeaCrittersHandler lists out paginated sea critters, is filterable and sortable, etc
func ListSeaCrittersHandler(w http.ResponseWriter, r *http.Request) {
	var j JSONResponse

	qs := r.URL.Query()

	catchable := util.Boolean(qs.Get("catchable"))
	seasonal := util.Boolean(qs.Get("seasonal"))
	timeOffset := qs.Get("time-offset")

	query := sea.QuerySelectSeaCritters{
		Caught:     util.Boolean(qs.Get("caught")),
		Donated:    util.Boolean(qs.Get("donated")),
		Hemisphere: qs.Get("hemisphere"),
		Offset:     util.Integer(qs.Get("offset")),
		Limit:      util.Integer(qs.Get("limit")),
		Sort:       util.Str(qs.Get("sort")),
		Dir:        util.Str(qs.Get("dir")),
	}

	if query.Caught != nil {
		query.CaughtIds = qs["caught-id"]
	}

	if query.Donated != nil {
		query.DonatedIds = qs["donated-id"]
	}

	now := time.Now()

	if timeOffset != constants.EmptyString {
		d, e := time.ParseDuration(timeOffset)
		if e != nil {
			logger.Error("couldn't parse time offset: %s", e)
		} else {
			now = now.Add(d)
		}
	}

	if catchable != nil {
		h := now.Hour()
		query.Hour = &h
	}
	if seasonal != nil || catchable != nil {
		m := int(now.Month())
		query.Month = &m
	}

	seaCritters, err := query.Execute()
	if err != nil {
		j = CreateJSONError(http.StatusInternalServerError, "error selecting sea critters: %s", err)
	} else {
		t, err := query.GetTotal()
		if err != nil {
			logger.Error("couldn't get total for ListSeaCrittersHandler: %s", err)
		}
		var o, l int
		if query.Offset != nil {
			o = *query.Offset
		}
		if query.Limit != nil {
			l = *query.Limit
		}
		j = JSONObject{
			Object: PaginationObject{
				Items:  seaCritters,
				Other:  map[string]interface{}{"time-used": now},
				Offset: o,
				Limit:  l,
				Total:  t,
			},
		}
	}

	WriteJSON(w, j)
}

// GetSeaCritterHandler gets a sea critter
func GetSeaCritterHandler(w http.ResponseWriter, r *http.Request) {
	var j JSONResponse

	name := mux.Vars(r)["id"]

	query := sea.QueryGetSeaCritter{}

	critterID, err := strconv.Atoi(name)
	if err != nil {
		query.Name = &name
	} else {
		query.CritterID = &critterID
	}

	var seaCritter models.NHSeaCritter
	seaCritter, err = query.Execute()
	if err != nil {
		j = CreateJSONError(http.StatusInternalServerError, "error getting sea critter %s: %s", name, err)
	} else {
		j = JSONObject{Object: seaCritter}
	}

	WriteJSON(w, j)
}
