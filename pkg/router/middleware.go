package router

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/logrusorgru/aurora/v4"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/metrics"
)

// DefaultAllowedOrigins is the default value used for the CrossDomainAllowedOrigins variable
const DefaultAllowedOrigins = "*"

// CrossDomainAllowedOrigins holds the config for allowing cross domain access
var CrossDomainAllowedOrigins = DefaultAllowedOrigins

// TimerMiddleware is a middleware that logs the time taken for every request
func TimerMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s := metrics.Start()

		h.ServeHTTP(w, r)
		label := fmt.Sprintf("%s %s", r.Method, aurora.Green(r.RequestURI).String())

		metrics.End(s, label, "false")
	})
}

// JSONResponseMiddleware is a middleware that sets up common response headers for JSON rest apis
func JSONResponseMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
		w.Header().Set("Pragma", "no-cache")
		w.Header().Set("Expires", "0")
		h.ServeHTTP(w, r)
	})
}

// CrossOriginMiddleware is a middleware that denies requests the come from outside a set of allowed domains
func CrossOriginMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		incomingOrigin := r.Header.Get("Origin")
		if CrossDomainAllowedOrigins == DefaultAllowedOrigins || strings.Contains(CrossDomainAllowedOrigins, incomingOrigin) {
			w.Header().Set("Access-Control-Allow-Origin", incomingOrigin)
			w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			h.ServeHTTP(w, r)
		} else {
			WriteJSON(w, CreateJSONError(http.StatusBadGateway, "You are trying to access this from a forbidden origin"))
		}
	})
}
