package router

import (
	"net/http"

	"gitlab.com/museum-buddy/museum-buddy-api/pkg/database/art"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/util"
)

// ListArtHandler lists out paginated art, is filterable and sortable, etc
func ListArtHandler(w http.ResponseWriter, r *http.Request) {
	var j JSONResponse

	qs := r.URL.Query()

	query := art.QuerySelectArt{
		Donated: util.Boolean(qs.Get("donated")),
		Offset:  util.Integer(qs.Get("offset")),
		Limit:   util.Integer(qs.Get("limit")),
		Sort:    util.Str(qs.Get("sort")),
		Dir:     util.Str(qs.Get("dir")),
	}

	if query.Donated != nil {
		query.DonatedIds = qs["donated-id"]
	}

	art, err := query.Execute()
	if err != nil {
		j = CreateJSONError(http.StatusInternalServerError, "error selecting art: %s", err)
	} else {
		t, err := query.GetTotal()
		if err != nil {
			logger.Error("couldn't get total for ListArtHandler: %s", err)
		}
		var o, l int
		if query.Offset != nil {
			o = *query.Offset
		}
		if query.Limit != nil {
			l = *query.Limit
		}
		j = JSONObject{
			Object: PaginationObject{
				Items:  art,
				Offset: o,
				Limit:  l,
				Total:  t,
			},
		}
	}

	WriteJSON(w, j)
}
