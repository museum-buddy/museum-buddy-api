package router

import (
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/config"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/util"
)

// InitRoutes sets up all the routes of the api
func InitRoutes() *mux.Router {

	sf := mux.NewRouter().PathPrefix("/v1").Subrouter().StrictSlash(true)

	sf.HandleFunc("/info", InfoHandler).Methods(http.MethodGet)
	sf.HandleFunc("/time-offset", CalculateOffsetHandler).Methods(http.MethodGet)
	sf.HandleFunc("/time-from-offset", CalculateTimeFromOffsetHandler).Methods(http.MethodGet)

	nh := sf.PathPrefix("/nh").Subrouter().StrictSlash(true)
	nh.HandleFunc("/insect/{id}", GetInsectHandler).Methods(http.MethodGet)
	nh.HandleFunc("/insect/", ListInsectsHandler).Methods(http.MethodGet)
	nh.HandleFunc("/fish/{id}", GetFishHandler).Methods(http.MethodGet)
	nh.HandleFunc("/fish/", ListFishHandler).Methods(http.MethodGet)
	nh.HandleFunc("/sea-critter/{id}", GetSeaCritterHandler).Methods(http.MethodGet)
	nh.HandleFunc("/sea-critter/", ListSeaCrittersHandler).Methods(http.MethodGet)
	nh.HandleFunc("/art/", ListArtHandler).Methods(http.MethodGet)
	nh.HandleFunc("/fossil/", ListFossilsHandler).Methods(http.MethodGet)

	sf.NotFoundHandler = http.HandlerFunc(NotFoundHandler)

	sf.Use(TimerMiddleware, JSONResponseMiddleware, CrossOriginMiddleware)

	return sf
}

// InfoHandler writes out information to the user about this api
func InfoHandler(w http.ResponseWriter, r *http.Request) {
	j := JSONObject{Object: map[string]interface{}{
		"name":      "Museum Buddy API",
		"requested": time.Now(),
		"env":       config.Values.APIEnv,
		"version":   config.Values.APIVersion,
	}}
	WriteJSON(w, j)
}

// CalculateOffsetHandler Calculates offset from system time
func CalculateOffsetHandler(w http.ResponseWriter, r *http.Request) {
	systemTime := util.Datetime(r.URL.Query().Get("system-time"))
	var offset string
	if systemTime != nil {
		offset = util.CalculateOffset(*systemTime)
	} else {
		offset = util.CalculateOffset(time.Now())
	}
	WriteJSON(w, JSONObject{Object: map[string]interface{}{
		"time-offset": offset,
	}})
}

// CalculateTimeFromOffsetHandler Calculates a time, given an offset
func CalculateTimeFromOffsetHandler(w http.ResponseWriter, r *http.Request) {
	var j JSONResponse
	duration, e := time.ParseDuration(r.URL.Query().Get("time-offset"))
	if e != nil {
		j = CreateJSONError(http.StatusBadRequest, "error parsing offset: %s", e)
	} else {
		d := time.Now()
		d = d.Add(duration)
		j = JSONObject{Object: map[string]interface{}{
			"time": d,
		}}
	}
	WriteJSON(w, j)
}
