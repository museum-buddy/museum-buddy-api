package router

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/constants"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/database/insects"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/models"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/util"
)

// ListInsectsHandler lists out paginated insects, is filterable and sortable, etc
func ListInsectsHandler(w http.ResponseWriter, r *http.Request) {
	var j JSONResponse

	qs := r.URL.Query()

	catchable := util.Boolean(qs.Get("catchable"))
	seasonal := util.Boolean(qs.Get("seasonal"))
	timeOffset := qs.Get("time-offset")

	query := insects.QuerySelectInsects{
		Caught:     util.Boolean(qs.Get("caught")),
		Donated:    util.Boolean(qs.Get("donated")),
		Hemisphere: qs.Get("hemisphere"),
		Offset:     util.Integer(qs.Get("offset")),
		Limit:      util.Integer(qs.Get("limit")),
		Sort:       util.Str(qs.Get("sort")),
		Dir:        util.Str(qs.Get("dir")),
	}

	if query.Caught != nil {
		query.CaughtIds = qs["caught-id"]
	}

	if query.Donated != nil {
		query.DonatedIds = qs["donated-id"]
	}

	now := time.Now()

	if timeOffset != constants.EmptyString {
		d, e := time.ParseDuration(timeOffset)
		if e != nil {
			logger.Error("couldn't parse time offset: %s", e)
		} else {
			now = now.Add(d)
		}
	}

	if catchable != nil {
		h := now.Hour()
		query.Hour = &h
	}
	if seasonal != nil || catchable != nil {
		m := int(now.Month())
		query.Month = &m
	}

	insects, err := query.Execute()
	if err != nil {
		j = CreateJSONError(http.StatusInternalServerError, "error selecting insects: %s", err)
	} else {
		t, err := query.GetTotal()
		if err != nil {
			logger.Error("couldn't get total for ListInsectsHandler: %s", err)
		}
		var o, l int
		if query.Offset != nil {
			o = *query.Offset
		}
		if query.Limit != nil {
			l = *query.Limit
		}
		j = JSONObject{
			Object: PaginationObject{
				Items:  insects,
				Other:  map[string]interface{}{"time-used": now},
				Offset: o,
				Limit:  l,
				Total:  t,
			},
		}
	}

	WriteJSON(w, j)
}

// GetInsectHandler lists out paginated insects, is filterable and sortable, etc
func GetInsectHandler(w http.ResponseWriter, r *http.Request) {
	var j JSONResponse

	name := mux.Vars(r)["id"]

	query := insects.QueryGetInsect{}

	critterID, err := strconv.Atoi(name)
	if err != nil {
		query.Name = &name
	} else {
		query.CritterID = &critterID
	}

	var insect models.NHInsect
	insect, err = query.Execute()
	if err != nil {
		j = CreateJSONError(http.StatusInternalServerError, "error getting insect %s: %s", name, err)
	} else {
		j = JSONObject{Object: insect}
	}

	WriteJSON(w, j)
}
