package router

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/logrusorgru/aurora/v4"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/constants"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/logger"
	"gitlab.com/museum-buddy/museum-buddy-api/pkg/metrics"
)

// NotFoundHandler is what happens when someone hits a bad endpoint
func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	s := metrics.Start()
	j := CreateJSONError(http.StatusNotFound, "no route for %s", r.RequestURI)
	w.Header().Set("Content-Type", "application/json")
	WriteJSON(w, j)
	metrics.End(s)
}

// WriteJSON is a shorthand for consistently writing JSON to the http.ResponseWriter
func WriteJSON(w http.ResponseWriter, j JSONResponse) {
	w.WriteHeader(j.GetCode())
	if l, err := w.Write(j.GetJSON()); err != nil {
		logger.Error("couldn't write json: %s", err.Error())
	} else {
		logger.Debug("wrote %d bytes", aurora.Cyan(l))
	}
}

// JSONResponse is an interface used for writing JSON responses easily to the http.ResponseWriter
type JSONResponse interface {
	GetCode() int
	GetJSON() []byte
}

// JSONError is a struct that implements the JSONResponse interface for
// gracefully handling errors
type JSONError struct {
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Data    map[string]interface{} `json:"data,omitempty"`
}

// GetCode returns the error code for the JSONError
func (j JSONError) GetCode() int {
	return j.Code
}

// GetJSON returns the error bytes
func (j JSONError) GetJSON() []byte {
	out, err := json.MarshalIndent(j, constants.EmptyString, constants.Space)
	if err != nil {
		logger.Error("couldn't marshal json for error response: %s", err.Error())
	}
	return out
}

// CreateJSONError is a shorthand for creating a JSONError with a formatted message.
func CreateJSONError(code int, msg string, vars ...interface{}) JSONError {
	if len(vars) > 0 {
		msg = fmt.Sprintf(msg, vars...)
	}
	logger.Warning(msg)
	return JSONError{Code: code, Message: msg}
}

// JSONAction implements the JSONResponse interface and is meant to be used with
// api endpoints that do not return data but instead perform an action
type JSONAction struct {
	Messages []string `json:"messages,omitempty"`
	Warnings []string `json:"warnings,omitempty"`
	Errors   []string `json:"errors,omitempty"`
}

// AddMessage will append a message to the Message slice, a convenience function
// This will also log the message to the info log
func (j *JSONAction) AddMessage(msg string, vars ...interface{}) {
	if len(vars) > 0 {
		msg = fmt.Sprintf(msg, vars...)
	}
	logger.Info(msg)
	j.Messages = append(j.Messages, msg)
}

// AddWarning will append a message to the Warning slice, a convenience function
// This will also log the message to the warning log
func (j *JSONAction) AddWarning(msg string, vars ...interface{}) {
	if len(vars) > 0 {
		msg = fmt.Sprintf(msg, vars...)
	}
	logger.Warning(msg)
	j.Warnings = append(j.Warnings, msg)
}

// AddError will append a message to the Error slice, a convenience function
// This will also log the message to the error log
func (j *JSONAction) AddError(msg string, vars ...interface{}) {
	if len(vars) > 0 {
		msg = fmt.Sprintf(msg, vars...)
	}
	logger.Error(msg)
	j.Errors = append(j.Errors, msg)
}

// GetCode returns the code for the JSONAction, should always be a 200
func (j *JSONAction) GetCode() int {
	return http.StatusOK
}

// GetJSON returns the action bytes
func (j *JSONAction) GetJSON() []byte {
	out, err := json.MarshalIndent(&j, constants.EmptyString, constants.Space)
	if err != nil {
		logger.Error("couldn't marshal json for action response: %s", err.Error())
	}
	return out
}

// JSONObject implements the JSONResponse interface for returning data
type JSONObject struct {
	Object interface{}
}

// GetCode returns the code for the JSONAction, should always be a 200
func (j JSONObject) GetCode() int {
	return http.StatusOK
}

// GetJSON returns the object bytes
func (j JSONObject) GetJSON() []byte {
	out, err := json.MarshalIndent(j.Object, constants.EmptyString, constants.Space)
	if err != nil {
		logger.Error("couldn't marshal json for object response: %s", err.Error())
	}
	return out
}

// PaginationObject is a struct with a common structure for paginated api endpoints.
type PaginationObject struct {
	Offset int                    `json:"offset"`
	Limit  int                    `json:"limit"`
	Total  int                    `json:"total"`
	Other  map[string]interface{} `json:"other-data,omitempty"`
	Items  interface{}            `json:"items"`
}
