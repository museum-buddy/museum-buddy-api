package logger

import (
	"errors"
	"fmt"
	"time"

	"github.com/logrusorgru/aurora/v4"
)

const (
	// LogLevelTrace is the token for TRACE level logs
	LogLevelTrace = "TRACE"
	// LogLevelDebug is the token for DEBUG level logs
	LogLevelDebug = "DEBUG"
	// LogLevelInfo is the token for INFO level logs
	LogLevelInfo = "INFO "
	// LogLevelWarning is the token for WARNING level logs
	LogLevelWarning = "WARN "
	// LogLevelError is the token for ERROR level logs
	LogLevelError = "ERROR"
)

var logLevel = LogLevelDebug
var format = "2006-01-02T15:04:05"

// Log will log to system out
// level will determine what log level is logged to
// If level is invalid, Log defaults to TRACE
// msg can be a formatted message ala `fmt.Sprintf`
// vars is an optional slice of things to be formatted into msg
func Log(level string, msg string, vars ...interface{}) {
	if !ValidLogLevel(level) {
		level = LogLevelTrace
	}
	if AcceptableLogLevel(level) {
		fm := msg
		if len(vars) > 0 {
			fm = fmt.Sprintf(msg, vars...)
		}
		now := time.Now()
		msg = fmt.Sprintf("[%s:%s] %s", colorizeLevel(level), aurora.Cyan(now.Format(format)), fm)
		fmt.Println(msg)
	}
}

// Error is a shorthand for logging at ERROR level
func Error(msg string, vars ...interface{}) {
	if len(vars) > 0 {
		Log(LogLevelError, msg, vars...)
	} else {
		Log(LogLevelError, msg)
	}
}

// Warning is a shorthand for logging at WARNING level
func Warning(msg string, vars ...interface{}) {
	if len(vars) > 0 {
		Log(LogLevelWarning, msg, vars...)
	} else {
		Log(LogLevelWarning, msg)
	}
}

// Info is a shorthand for logging at INFO level
func Info(msg string, vars ...interface{}) {
	if len(vars) > 0 {
		Log(LogLevelInfo, msg, vars...)
	} else {
		Log(LogLevelInfo, msg)
	}
}

// Debug is a shorthand for logging at DEBUG level
func Debug(msg string, vars ...interface{}) {
	if len(vars) > 0 {
		Log(LogLevelDebug, msg, vars...)
	} else {
		Log(LogLevelDebug, msg)
	}
}

// Trace is a shorthand for logging at TRACE level
func Trace(msg string, vars ...interface{}) {
	if len(vars) > 0 {
		Log(LogLevelTrace, msg, vars...)
	} else {
		Log(LogLevelTrace, msg)
	}
}

func colorizeLevel(l string) aurora.Value {
	switch l {
	case LogLevelTrace:
		return aurora.BrightGreen(l)
	case LogLevelDebug:
		return aurora.Cyan(l)
	case LogLevelWarning:
		return aurora.Yellow(l)
	case LogLevelError:
		return aurora.Red(l)
	}
	return aurora.White(l)
}

// SetDateFormat will set the logged date format to the supplied format.
// If the format is invalid, an error will be returned
func SetDateFormat(f string) error {
	fd := time.Time{}.Format(format)
	if _, e := time.Parse(f, fd); e != nil {
		return e
	}
	format = f
	return nil
}

// SetLogLevel sets the supplied level to the application's log level if valid.
// returns an error if the supplied level is not valid
func SetLogLevel(l string) error {
	if ValidLogLevel(l) {
		logLevel = l
		return nil
	}
	return errors.New("Invalid log level " + l)
}

// ValidLogLevel will return true if the supplied level is a valid level of logging.
func ValidLogLevel(l string) bool {
	return l == LogLevelTrace || l == LogLevelDebug || l == LogLevelInfo || l == LogLevelWarning || l == LogLevelError
}

// AcceptableLogLevel returns true if the supplied log level can be logged based on the application's current log level.
func AcceptableLogLevel(l string) bool {
	return logLevel == LogLevelTrace ||
		(logLevel == LogLevelDebug && l != LogLevelTrace) ||
		(logLevel == LogLevelInfo && l != LogLevelTrace && l != LogLevelDebug) ||
		(logLevel == LogLevelWarning && l != LogLevelTrace && l != LogLevelDebug && l != LogLevelInfo) ||
		(logLevel == LogLevelError && l == LogLevelError)
}
