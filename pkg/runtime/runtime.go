package runtime

import (
	"runtime"

	"gitlab.com/museum-buddy/museum-buddy-api/pkg/constants"
)

// MyCaller returns the caller name of the function that called it.
// skip is an optional parameter that skips levels of the call stack (default 0)
func MyCaller(skip ...int) string {
	doSkip := 0
	if len(skip) > 0 {
		doSkip = skip[0]
	}

	fpcs := make([]uintptr, 1)

	n := runtime.Callers(doSkip, fpcs)
	if n == 0 {
		return constants.EmptyString
	}
	fun := runtime.FuncForPC(fpcs[0] - 1)
	if fun == nil {
		return constants.EmptyString
	}
	name := fun.Name()
	return name
}
