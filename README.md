# Museum Buddy API

This project contains the code and documentation for the Museum Buddy API.
It is written in golang and makes use of the gorilla MUX package.
This API is self-contained, and does NOT require a database.

All stateful data is expected to be passed into each API call and is calculated on the fly.
It's incredibly inefficient but allows the API to stand on it's own and be stateless itself.